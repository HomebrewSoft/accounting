{
    "name": "Account Advance Payment",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
    ],
    "data": [
        # security
        # data
        "views/account_payment.xml",
        "views/res_partner.xml",
        # reports
        # views
    ],
}
