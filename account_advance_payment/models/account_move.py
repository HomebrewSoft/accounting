from odoo import _, models


class AccountMove(models.Model):
    _inherit = "account.move"

    def js_assign_outstanding_line(self, line_id):
        res = super(AccountMove, self).js_assign_outstanding_line(line_id)
        line = self.env["account.move.line"].browse(line_id)
        if not line or not line.payment_id.is_advance_payment:
            return res
        currency_id = (
            line.payment_id.currency_id.id
            if line.payment_id.currency_id != self.currency_id
            else False
        )
        provider_data = {
            "name": _("Counter Client"),
            "amount_currency": 1,
            "currency_id": currency_id,
            "debit": line.payment_id.amount,
            "credit": 0,
            "date_maturity": self.invoice_date,
            "partner_id": self.partner_id.commercial_partner_id.id,
            "account_id": line.payment_id.destination_account_id.id,
            "exclude_from_invoice_tab": True,
        }
        advance_data = {
            "name": _("Advance Payment"),
            "amount_currency": 1,
            "currency_id": currency_id,
            "debit": 0,
            "credit": line.payment_id.amount,
            "date_maturity": self.invoice_date,
            "partner_id": self.partner_id.commercial_partner_id.id,
            "account_id": line.payment_id.partner_id.property_account_advance_id.id,
            "exclude_from_invoice_tab": True,
        }
        self.line_ids = [
            (0, None, provider_data),
            (0, None, advance_data),
        ]
        return res
