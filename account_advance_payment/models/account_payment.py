from odoo import _, fields, models


class AccountPayment(models.Model):
    _inherit = "account.payment"

    is_advance_payment = fields.Boolean()

    def _prepare_payment_moves(self):
        res = super(AccountPayment, self)._prepare_payment_moves()
        for payment in self:
            if not payment.is_advance_payment:
                continue
            partner = payment.partner_id.with_context(force_company=payment.company_id.id)
            company_currency = payment.company_id.currency_id

            # Compute amounts.
            write_off_amount = (
                payment.payment_difference_handling == "reconcile"
                and -payment.payment_differenceor
                or 0.0
            )
            if payment.payment_type in ("outbound", "transfer"):
                counterpart_amount = payment.amount
            else:
                counterpart_amount = -payment.amount

            # Manage currency.
            if payment.currency_id == company_currency:
                # Single-currency.
                balance = counterpart_amount
                write_off_balance = write_off_amount
                counterpart_amount = write_off_amount = 0.0
                currency_id = False
            else:
                # Multi-currencies.
                balance = payment.currency_id._convert(
                    counterpart_amount,
                    company_currency,
                    payment.company_id,
                    payment.payment_date,
                )
                write_off_balance = payment.currency_id._convert(
                    write_off_amount,
                    company_currency,
                    payment.company_id,
                    payment.payment_date,
                )
                currency_id = payment.currency_id.id

            if (
                payment.journal_id.currency_id
                and payment.currency_id != payment.journal_id.currency_id
            ):
                # Custom currency on journal.
                if payment.journal_id.currency_id == company_currency:
                    # Single-currency
                    liquidity_line_currency_id = False
                else:
                    liquidity_line_currency_id = payment.journal_id.currency_id.id
                liquidity_amount = company_currency._convert(
                    balance,
                    payment.journal_id.currency_id,
                    payment.company_id,
                    payment.payment_date,
                )
            else:
                # Use the payment currency.
                liquidity_line_currency_id = currency_id
                liquidity_amount = counterpart_amount

            # Compute 'name' to be used in receivable/payable line.
            rec_pay_line_name = ""
            if payment.payment_type == "transfer":
                rec_pay_line_name = payment.name
            else:
                if payment.partner_type == "customer":
                    if payment.payment_type == "inbound":
                        rec_pay_line_name += _("Customer Payment")
                    elif payment.payment_type == "outbound":
                        rec_pay_line_name += _("Customer Credit Note")
                elif payment.partner_type == "supplier":
                    if payment.payment_type == "inbound":
                        rec_pay_line_name += _("Vendor Credit Note")
                    elif payment.payment_type == "outbound":
                        rec_pay_line_name += _("Vendor Payment")
                if payment.invoice_ids:
                    rec_pay_line_name += ": %s" % ", ".join(payment.invoice_ids.mapped("name"))

            for move in res:
                provider_data = {
                    "name": _("Counter Client"),
                    "amount_currency": (
                        counterpart_amount + write_off_amount if currency_id else 0.0
                    ),
                    "currency_id": currency_id,
                    "credit": balance + write_off_balance > 0.0
                    and balance + write_off_balance
                    or 0.0,
                    "debit": balance + write_off_balance < 0.0
                    and -balance - write_off_balance
                    or 0.0,
                    "date_maturity": payment.payment_date,
                    "partner_id": payment.partner_id.commercial_partner_id.id,
                    "account_id": payment.destination_account_id.id,
                    "payment_id": payment.id,
                }
                advance_data = {
                    # advance account line
                    "name": _("Advance Payment"),
                    "amount_currency": -liquidity_amount if liquidity_line_currency_id else 0.0,
                    "currency_id": liquidity_line_currency_id,
                    "credit": balance < 0.0 and -balance or 0.0,
                    "debit": balance > 0.0 and balance or 0.0,
                    "date_maturity": payment.payment_date,
                    "partner_id": payment.partner_id.commercial_partner_id.id,
                    "account_id": partner.property_account_advance_id.id,
                    "payment_id": payment.id,
                }
                move["line_ids"].append((0, 0, provider_data))
                move["line_ids"].append((0, 0, advance_data))
        return res
