from odoo import fields, models


class Partner(models.Model):
    _inherit = "res.partner"

    property_account_advance_id = fields.Many2one(
        comodel_name="account.account",
        required=True,
        domain=[
            ("user_type_id.internal_group", "=", "asset"),
        ],
        string="Account Advance Payment",
    )
